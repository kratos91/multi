package main

import (
	"bitbucket.org/kratos91/multi/deadlocks_train/common"
	"bitbucket.org/kratos91/multi/deadlocks_train/hierarchy"
	"github.com/hajimehoshi/ebiten"
	"log"
	"sync"
)

const trainLength = 70

var (
	trains        [4]*common.Train
	intersections [4]*common.Intersection
)

func update(screen *ebiten.Image) error {
	if !ebiten.IsDrawingSkipped() {
		DrawTracks(screen)
		DrawIntersections(screen)
		DrawTrains(screen)
	}
	return nil
}

func main() {
	for i := 0; i < 4; i++ {
		trains[i] = &common.Train{
			Id:          i,
			TrainLength: trainLength,
			Front:       0,
		}
	}

	for i := 0; i < 4; i++ {
		intersections[i] = &common.Intersection{
			Id:       i,
			Mutex:    sync.Mutex{},
			LockedBy: -1,
		}
	}

	go hierarchy.MoveTrain(trains[0], 300, []*common.Crossing{
		{Position: 125, Intersection: intersections[0]},
		{Position: 175, Intersection: intersections[1]},
	})

	go hierarchy.MoveTrain(trains[1], 300, []*common.Crossing{
		{Position: 125, Intersection: intersections[1]},
		{Position: 175, Intersection: intersections[2]},
	})
	go hierarchy.MoveTrain(trains[2], 300, []*common.Crossing{
		{Position: 125, Intersection: intersections[2]},
		{Position: 175, Intersection: intersections[3]},
	})
	go hierarchy.MoveTrain(trains[3], 300, []*common.Crossing{
		{Position: 125, Intersection: intersections[3]},
		{Position: 175, Intersection: intersections[0]},
	})

	if err := ebiten.Run(update, 320, 320, 3, "Trains in a box"); err != nil {
		log.Fatal(err)
	}
}
