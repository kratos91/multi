module bitbucket.org/kratos91/multi

go 1.16

require (
	github.com/hajimehoshi/ebiten v1.12.12
	github.com/hajimehoshi/ebiten/v2 v2.2.3
)
